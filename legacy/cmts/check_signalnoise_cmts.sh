#!/bin/bash

COMUNITY=$2
HOST=$1
TTL=0
COUNT=0
#TEXT=""
#PERFDATA=""

for i in $(snmpwalk -v1 -c ${COMUNITY} ${HOST} .1.3.6.1.2.1.10.127.1.1.4.1.5 -Oqv) ; do
  VALUE=$(echo 2k $i 10 /p | dc) 
  TEXT="${TEXT}PORT_${COUNT} ${VALUE} \n"
  PERFDATA="${PERFDATA}PORT_${COUNT}=${VALUE} "
  COUNT=$((COUNT+1))
done

echo -e "SigQSignalNoise: \n ${TEXT} | ${PERFDATA} " 
$(exit 0)

