#!/bin/bash
# Script to check mobile registered
# Written by: Dorance Martinez (dorancemc@gmail.com)
# Version 1.0 - 11nov2014
#

USAGE="`basename $0` [-u]<user_name> [-p]<password> [-H]<hostname> [-P]<port> "
username=""
password=""
hostname=""
imei=""
if [[ $# -lt 8 ]]
then
	echo ""
	echo "Wrong Syntax: `basename $0` $*"
	echo ""
	echo "Usage: $USAGE"
	echo ""
	exit 0
fi
while [[ $# -gt 0 ]]
  do
        case "$1" in
               -u)
               shift
               username=$1
        ;;
               -p)
               shift
               password=$1
        ;;
               -H)
               shift
               hostname=$1
        ;;
               -P)
               shift
               port=$1
        ;;
        esac
        shift
  done

# estado=`curl -s -u $username:$password -k http://$hostname/enMobileInfo.htm | sed 's/<\/tr[^>]*>/\n/g' | grep $imei |  sed 's/<\/t[dh][^>]*><t[dh][^>]*>/,/g' | cut -d"," -f5`;
#estado=`curl -s -u $username:$password -k http://$hostname/enMobileInfo.htm | sed 's/<\/tr[^>]*>/\n/g' | grep "<tr><td>${port}</td><td>GSM</td><td>" |  sed 's/<\/t[dh][^>]*><t[dh][^>]*>/,/g' | cut -d"," -f4,5`;
total=`curl -s -u $username:$password -k http://$hostname/enMobileInfo.htm | sed 's/<\/tr[^>]*>/\n/g' | grep "<tr><td>${port}</td><td>GSM</td><td>" |  sed 's/<\/t[dh][^>]*><t[dh][^>]*>/,/g' | cut -d"," -f4,5`;
imei=`echo $total | cut -d"," -f1`
estado=`echo $total | cut -d"," -f2`

if [ "$estado" != "Mobile Registered" ];
	then
		echo "CRITICAL: PORT=$port, IMEI=$imei, Estatus=$estado | estado=1"
		exit 2
fi
if [ "$estado" == "Mobile Registered" ];
	then
		echo "OK: PORT=$port, IMEI=$imei, Estatus=$estado | estado=0"
		exit 0
fi


