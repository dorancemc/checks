#!/bin/bash
# Script to check mobile from file
# Written by: Dorance Martinez (dorancemc@gmail.com)
# Version 1.0 - 03dic2014
#

USAGE="`basename $0` [-F]<folder> [-H]<host> [-P]<port> -m<minutes> "
folder=""
host=""

if [[ $# -lt 8 ]]
then
        echo ""
        echo "Wrong Syntax: `basename $0` $*"
        echo ""
        echo ""
        exit 0
fi
while [[ $# -gt 0 ]]
  do
        case "$1" in
               -F)
               shift
               folder=$1
        ;;
               -H)
               shift
               host=$1
        ;;
               -P)
               shift
               port=$1
        ;;
               -m)
               shift
               mint=$1
        ;;
        esac
        shift
  done

dia=$(date +%d)
mes=$(date +%m)
anno=$(date +%Y)
fecha="$anno""-""$mes""-""$dia"
logfile="${folder}/${fecha}.log"

rm -f /tmp/${host}_${port}_1.txt
mv /tmp/${host}_${port}_2.txt /tmp/${host}_${port}_1.txt
cat $logfile | grep ${host} | grep port:${port} | grep succ: | tail -n 1 >> /tmp/${host}_${port}_2.txt
succ=`diff -n --suppress-common-lines /tmp/${host}_${port}_1.txt /tmp/${host}_${port}_2.txt | grep "port:$port" | sed 's/<  /</g' | sed 's/< /</g' | awk '{ print $12}' | cut -d":" -f 2 | cut -d"#" -f 1`

if [ "${succ}" != "" ]; then
    if [ "${succ}" -gt 0 ]; then
        echo "OK: PORT=$port Succ=$succ | succ=$succ"
        exit 0
    else
        echo "WARNING: PORT=$port Succ=$succ | succ=$succ"
            exit 1
    fi
else
    date_in_file=`cat /tmp/${host}_${port}_1.txt | awk '{print $1 " " $2 " " $3}'`
    date1=`date +%s --date="$date_in_file"`
    date2=`date +%s --date="$mint min ago"`
    if [ $date2 -lt $date1 ]; then
        echo "OK: PORT=$port sin actualizaciones desde $date_in_file "
        exit 0
    else
        echo "CRITICAL: PORT=$port sin actualizaciones desde $date_in_file "
        exit 2
    fi
fi

