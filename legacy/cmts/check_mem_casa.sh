#!/bin/bash

if [ "$3" = "-w" ] && [ "$4" -gt "0" ] && [ "$5" = "-c" ] && [ "$6" -gt "0" ]; then

  HOST=$1
  COMUNITY=$2
  memTotal_b=`snmpget -v1 -c ${COMUNITY} ${HOST} 1.3.6.1.4.1.20858.10.13.1.1.1.1.2 -Oqv`
  memFree_b=`snmpget -v1 -c ${COMUNITY} ${HOST} 1.3.6.1.4.1.20858.10.13.1.1.1.3.2 -Oqv`
  memUsed_b=`snmpget -v1 -c ${COMUNITY} ${HOST} 1.3.6.1.4.1.20858.10.13.1.1.1.2.2 -Oqv`
  memUsedPrc=$((($memUsed_b*100)/$memTotal_b))

  if [ "$memUsedPrc" -ge "$6" ]; then
    echo "Memory: CRITICAL Total: $memTotal_b B - Used: $memUsed_b B - $memUsedPrc% used!|MEM=$memUsed_b;;;;$memTotal_b"
    $(exit 2)
  elif [ "$memUsedPrc" -ge "$4" ]; then
    echo "Memory: WARNING Total: $memTotal_b B - Used: $memUsed_b B - $memUsedPrc% used!|MEM=$memUsed_b;;;;$memTotal_b"
    $(exit 1)
  else
    echo "Memory: OK Total : $memTotal_b B - Used: $memUsed_b B - $memUsedPrc% used|MEM=$memUsed_b;;;;$memTotal_b"
    $(exit 0)
  fi

else
  echo "$0 v1.1"
  echo ""
  echo "Usage:"
  echo "$0 <HOST> <COMUNITY> -w <warnlevel> -c <critlevel>"
  echo ""
  echo "warnlevel and critlevel is percentage value without %"
  echo ""
  exit
fi
