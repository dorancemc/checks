#!/bin/bash

COMUNITY=$2
HOST=$1
TTL=0
COUNT=0
#TEXT=""
#PERFDATA=""

for i in $(snmpwalk -v1 -c ${COMUNITY} ${HOST} 1.3.6.1.4.1.20858.10.12.1.1.1.1 -Oqv) ; do 
  TTL=$((TTL + i))
  TEXT="${TEXT}PORT_${COUNT} $i \n"
  PERFDATA="${PERFDATA}PORT_${COUNT}=$i "
  COUNT=$((COUNT+1))
done

echo -e "Conteo de CM : Total = ${TTL} \n ${TEXT} | ${PERFDATA} " 
$(exit 0)

