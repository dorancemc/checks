<?php
// Copyright (C) 2017 - DMC Ingenieria SAS. http://dmci.co 
// Author: Jaime Andres Cardona - jacardona@outlook.com
// SPDX-License-Identifier: Apache-2.0

// *******************************************************
// Variables globales
// intTipoEjecucion=1 => por navevgador WEB
// intTipoEjecucion=2 => por SHELL o linea de comandos
$intTipoEjecucion=2;

$strUrl='';
$intProcesoFinalizadoOk=1;
$strProcesoFinalizado="";

// *******************************************************
// MAIN
$strBajarLinea="";
if($intTipoEjecucion==1)// Capturando parametros GET - si se ejecuta por web
{
	$strBajarLinea="<br>";
	extract($_GET);
}

if($intTipoEjecucion==2) // Capturando parametros por argumentos - si se ejecuta por shell
{
	foreach ($argv as $item)
	{
		$arrItem=explode("=",$item);
		if(sizeof($arrItem)==2)
		{
			${$arrItem[0]}=$arrItem[1];
		}
	}
}

if(!isset($depuracion))
{
	$depuracion=0;
}

if(!isset($stUser) || !isset($strPassword) || !isset($strCelular) || !isset($strMsg))
{
	menu();
	exit;
}

function menu()
{
	global $strBajarLinea;
	
	echo "$strBajarLinea\n";
	echo "Menu - Parametros esperados"."$strBajarLinea\n";
	echo "stUser : Usuario"."$strBajarLinea\n";
	echo "strPassword : Clave"."$strBajarLinea\n";
	echo "strCelular : Celular"."$strBajarLinea\n";
	echo "strMsg : Mensaje\n"."$strBajarLinea";
	echo "depuracion : 1=Ver mensajes en pantalla, 0=valor por defecto, sin mensajes"."$strBajarLinea\n";
	echo "Ejemplos"."$strBajarLinea\n";

	echo "$strBajarLinea\n";
	echo "URL (script intTipoEjecucion=1)\n"."$strBajarLinea";
	echo "http(s)://domain.com/send_sms_soap.php?stUser=user&strPassword=password&strCelular=###&strMsg=mensaje para el sms\n"."$strBajarLinea";

	echo "$strBajarLinea\n";
	echo "URL (script intTipoEjecucion=1) - Activar mensajes en pantalla de debug\n"."$strBajarLinea";
	echo "http(s)://domain.com/send_sms_soap.php?stUser=user&strPassword=password&strCelular=###&strMsg=mensaje para el sms&depuracion=1\n"."$strBajarLinea";

	echo "$strBajarLinea\n";
	echo "Shell (script intTipoEjecucion=2)\n"."$strBajarLinea";
	echo "/usr/bin/php -q /path/send_sms_soap.php stUser=user strPassword=password strCelular=### strMsg='mensaje para el sms'\n"."$strBajarLinea";
	
	echo "$strBajarLinea\n";
	
}


if($depuracion==1)
{
	echo "Mensajes en pantalla activados$strBajarLinea\n";
	// impresion variables
	echo "$strBajarLineaParametros capturados$strBajarLinea\n";
	echo "stUser=$stUser$strBajarLinea\n";
	echo "strPassword=$strPassword$strBajarLinea\n";
	echo "strCelular=$strCelular$strBajarLinea\n";
	echo "strMsg=$strMsg$strBajarLinea\n";
	echo "depuracion=$depuracion$strBajarLinea\n";
	echo "$strBajarLinea\n";
}

/* Mostrar todos los errores */
ini_set('display_errors', $depuracion);

if($depuracion==1)
	echo "Ejecutando$strBajarLinea\n";

try {
	$ini = microtime(true);
	/* Crear un cliente (endpoint) - el primer parametro es la ubicacion del WSDL, el segundo indica que se debe utilizar WSDL */
	$soap = new SoapClient($strUrl);
	$ms1 = microtime(true);

	/* 
	 * Aqui usamos el metodo newSendSMS() que recibe, un usuario, un password, la lista de telefonos destinatarios, y el texto a enviar.
	 */
	$res = $soap->newSendSMS($stUser, $strPassword, $strCelular, $strMsg);
	$arrResultado = json_decode(json_encode($res), true);

	$ms2 = microtime(true);
	if($depuracion==1)
	{
		echo "Resultado: " . print_r($res, true) . "$strBajarLinea\n";		
		echo "status=".$arrResultado['status']."$strBajarLinea\n";
		echo "idMessage=".$arrResultado['idMessage']."$strBajarLinea\n";
		echo "desc=".$arrResultado['desc']."$strBajarLinea\n";
		echo "totalPhones=".$arrResultado['totalPhones']."$strBajarLinea\n";
		echo "badPhones=".$arrResultado['badPhones']."$strBajarLinea\n";
	}
	
	if($arrResultado['status']!=1)
		$intProcesoFinalizadoOk=0;
	$strProcesoFinalizado=$strProcesoFinalizado."newSendSMS=".$arrResultado['desc'];
	
	$key = $res->idMessage;

	// Tiempo de ejecucion
	if($depuracion==1)
		echo "WSDL milestone: " . ($ms1 - $ini) . " us. SOAP milestone: " . ($ms2 - $ini) ." us$strBajarLinea\n";

	if($depuracion==1)
		echo "Esperando 10 segundos para revisar estado de envio. Ticket=$key$strBajarLinea\n";
	sleep(10);

	$res = $soap->getInfoSMS($stUser, $strPassword, $key, 'all');
	$arrResultado = json_decode(json_encode($res), true);

	/* Mostrar resultado  (ver documentacion)*/
	if($depuracion==1)
	{
		echo "Resultado: " . print_r($res, true) . "$strBajarLinea\n";		
		echo "status=".$arrResultado['status']."$strBajarLinea\n";
		echo "filter=".$arrResultado['filter']."$strBajarLinea\n";
		echo "date=".$arrResultado['date']."$strBajarLinea\n";
		echo "count=".$arrResultado['count']."$strBajarLinea\n";
		echo "desc=".$arrResultado['desc']."$strBajarLinea\n";
	}
	
	if($arrResultado['status']!=1)
		$intProcesoFinalizadoOk=0;
	$strProcesoFinalizado=$strProcesoFinalizado.",getInfoSMS=".$arrResultado['desc'];
	
	// echo "$strBajarLineaResultado salida$strBajarLinea";
	// $explodedString = explode(',', $res->Object);
	// var_dump($explodedString)."$strBajarLinea";
}
catch (Exception $e) {
	if($depuracion==1)
	{
		echo "Exception presentada durante ejecucion SOAP: \n";
		echo $e->getMessage();
	}
}

if($arrResultado['status']==1) // Proceso sin errores
	echo "Proceso finalizado correctamente, mensajes=".$strProcesoFinalizado."$strBajarLinea\n";
else
	echo "Proceso finalizado con errores, mensajes=".$strProcesoFinalizado."$strBajarLinea\n";
?>

